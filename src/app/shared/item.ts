import {Comment} from './comment';

export class Item {
  name: string;
  image: string;
  category: string;
  label: string;
  price: string;
  description: string;
  comments: Comment[];
  id: number;
  featured: boolean;

}
